import re2 as re

class ReportFile:
  def __init__(self, name) -> None:
    self.name = name
    self.reports = []
    self.__report_matcher = re.compile("<report>(.+?)</report>", re.DOTALL)

  def load(self, file):
    buffer = file.read()
    file.close()
    matches = self.__report_matcher.findall(buffer)
    if matches:
      self.reports.extend(list(map(str.strip, matches)))

  def size(self) -> int:
    return len(self.reports)
