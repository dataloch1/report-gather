import pytest
from report_file import ReportFile
from io import SEEK_END, StringIO

class TestReportFile:
  @pytest.fixture
  def document1(self):
    return """<report>some more text</report>"""

  @pytest.fixture
  def document2(self):
    return """<report>
    some more text
    </report>"""

  @pytest.fixture
  def file1(self, document1):
    return StringIO(document1)

  @pytest.fixture
  def file2(self, document2):
    return StringIO(document2)

  @pytest.fixture
  def file3(self, document1, document2):
    io = StringIO(document1)
    io.seek(0, SEEK_END)
    io.write(document2)
    io.seek(0)

    return io

  def test_report_breaks(self, file1):
    r = ReportFile("TestReport")
    r.load(file1)

    assert r.size() == 1

  def test_report_breaks2(self, file2):
    r = ReportFile("TestReport")
    r.load(file2)

    assert r.size() == 1

  def test_report_breaks3(self, file3):
    r = ReportFile("TestReport")
    r.load(file3)

    assert r.size() == 2

  def test_report_cleans_string(self, file2):
    r = ReportFile("TestReport")
    r.load(file2)

    assert "\n" not in r.reports[0]
