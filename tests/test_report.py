import pytest
from report import Report

class TestReport:
  def test_report1(self):
    text = "Lorem eposn"
    r = Report(text, "")

    assert r.report == text
    
  def test_report_length(self):
    text = "Lorem eposn"
    r = Report(text, "")

    assert r.size() == len(text)
  
  def test_report_name(self):
    text = "Lorem eposn"
    r = Report(text, "name")

    assert r.name == "name"
