from manage_list import ManageList
import random

class TestReportStructure:
  def __init__(self) -> None:
    self.random_length = random.randint(1, 99)

  def size(self):
    return self.random_length

class TestReportFileStructure:
  def __init__(self) -> None:
    self.reports = []
    random_length = random.randint(1, 5)
    for i in range(random_length):
      self.reports.append(TestReportStructure())

  def size(self):
    return random.randint(1, 5)

class TestManageList:
  def test_sorted_list(self):
    list2 = []
    m = ManageList()
    for i in range(1,5):      
      trs = TestReportFileStructure()
      list2.extend(trs.reports)
      m.add_report_file(trs)

    def sort_size(e):
      return e.size()

    list1 = m.sorted()
    list2.sort(key=sort_size)

    assert list1[1] == list2[1]
    assert list1[2] == list2[2]
