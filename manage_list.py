class ManageList:
  def __init__(self) -> None:
    self.reports = []

  def add_report_file(self, report_file):
    self.reports.append(report_file)

  def sorted(self):
    def by_size(e):
      return e.size()

    self.reports.sort(key=by_size)

    return self.reports
