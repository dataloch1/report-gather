#!/usr/bin/env python3

import argparse
from os import listdir
from os.path import isfile, join, isdir
from report_file import ReportFile
from manage_list import ManageList

def version():
  return "0.9.5"

def epilog():
  return f"Version: {version()} DataLoch (c) 2022"

parser = argparse.ArgumentParser(description='Gathers reports into bundles for analysis', epilog=epilog())
parser.add_argument('--directory', help="Direct of report files", default=".")
parser.add_argument('output', help="Directory to place report cohorts in")

args = parser.parse_args()
if not isdir(args.output):
  raise NotADirectoryError(f"{args.output} is not a valid directory")

onlyfiles = [f for f in listdir(args.directory) if isfile(join(args.directory, f)) and f.endswith(".txt")]

ml = ManageList()
for file in onlyfiles:
  r = ReportFile(file)
  r.load(open(join(args.directory, file), "r", encoding="UTF-8"))
  ml.add_report_file(r)

with open(join(args.output, 'report.txt'), "w") as f:
  for report in ml.sorted():
    f.write(f"In {report.name} size: {report.size()}\n")

