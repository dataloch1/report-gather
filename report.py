class Report:
  def __init__(self, report, name) -> None:
    self.report = report
    self.name = name

  def size(self) -> int:
    return len(self.report)
    